package hk.com.novare.employee.profile.employee.profile.controller;

import hk.com.novare.employee.profile.employee.profile.service.impl.EmployeeDaoServiceImpl;
import hk.com.novare.employee.profile.employee.profile.model.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class EmployeeController {

    @Autowired
    private EmployeeDaoServiceImpl employeeDaoServiceImpl;

    @GetMapping(value="/getAll", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Employee> getEmployees() {
        return employeeDaoServiceImpl.getAllEmployee();
    }

    @PostMapping(value="/addEmployee/{first_name}/{last_name}/{dept_id}")
    public void addEmployee(@PathVariable("first_name") String first_name,
                            @PathVariable("last_name") String last_name,
                            @PathVariable("dept_id") int dept_id){
        Employee e = new Employee(first_name, last_name, dept_id);
        employeeDaoServiceImpl.addEmployee(e);
    }

    @PostMapping(value="/addEmployee/")
    public void addEmployeeParam(@RequestParam("first_name") String first_name,
                                 @RequestParam("last_name") String last_name,
                                 @RequestParam("dept_id") int dept_id){
        Employee e = new Employee(first_name,last_name,dept_id);
        employeeDaoServiceImpl.addEmployee(e);
    }

    @PostMapping(value="/addEmployeeObject")
    public void addEmployeeParam(@RequestBody Employee e){
        employeeDaoServiceImpl.addEmployee(e);
    }

    @DeleteMapping(value="/deleteEmployee/{id}")
    public void deleteEmployee(@PathVariable("id") int id) throws  Exception{
        employeeDaoServiceImpl.deleteEmployee(id);
    }


    @DeleteMapping(value="/deleteEmployee")
    public void deleteEmployeeParam(@RequestParam("id") int id) throws  Exception{
        employeeDaoServiceImpl.deleteEmployee(id);
    }


    @PutMapping(value="/updateEmployee")
    public void updateEmployee(@RequestBody Employee e) throws Exception{
        employeeDaoServiceImpl.updateEmployee(e);
    }
}