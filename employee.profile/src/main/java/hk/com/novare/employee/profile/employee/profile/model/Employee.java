package hk.com.novare.employee.profile.employee.profile.model;

import javax.persistence.*;
import java.util.Date;

@Entity
public class Employee {
    @Id
    @GeneratedValue(strategy =  GenerationType.AUTO)
    private int id;
    private String first_name;
    private String last_name;
    @Temporal(TemporalType.TIMESTAMP)
    private Date date_hired;
    private int department_id;

    public Employee() {
    }

    public Employee(String first_name, String last_name, int dept_id) {
        this.first_name = first_name;
        this.last_name = last_name;
        this.department_id = dept_id;
        date_hired = new Date();
    }

    public Employee(int id, String first_name, String last_name, Date date_hired, int department_id) {
        this.id = id;
        this.first_name = first_name;
        this.last_name = last_name;
        this.date_hired = date_hired;
        this.department_id = department_id;
    }

    public Employee(int id, String first_name, String last_name, int department_id) {
        this.id = id;
        this.first_name = first_name;
        this.last_name = last_name;
        this.department_id = department_id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public Date getDate_hired() {
        return date_hired;
    }

    public void setDate_hired(Date date_hired) {
        this.date_hired = date_hired;
    }

    public int getDepartment_id() {
        return department_id;
    }

    public void setDepartment_id(int department_id) {
        this.department_id = department_id;
    }

}
