package hk.com.novare.employee.profile.employee.profile.dao;

import hk.com.novare.employee.profile.employee.profile.model.Employee;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EmployeeDao extends JpaRepository<Employee, Integer> {
}
