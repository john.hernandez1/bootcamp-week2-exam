package hk.com.novare.employee.profile.employee.profile.service.impl;

import hk.com.novare.employee.profile.employee.profile.dao.EmployeeDao;
import hk.com.novare.employee.profile.employee.profile.model.Employee;
import hk.com.novare.employee.profile.employee.profile.service.EmployeeDaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmployeeDaoServiceImpl implements EmployeeDaoService {

    @Autowired
    private EmployeeDao employeeDao;

    public Employee addEmployee(Employee e) {
        return employeeDao.save(e);
    }

    public List<Employee> getAllEmployee(){
        return(employeeDao.findAll());
    }

    public Employee getEmployeeById(int id) throws Exception{
        boolean isExisting = employeeDao.findById(id).isPresent();
        if (isExisting) {
            return (employeeDao.findById(id).get());
        }else{
            throw new Exception("Employee does not exist!");
        }
    }

    public int deleteEmployee(int id) throws Exception{

        boolean isExisting = employeeDao.findById(id).isPresent();
        if(isExisting){
            employeeDao.deleteById(id);
            return (id);
        }else {
            throw new Exception("Employee does not exist!");
        }


    }
    public Employee updateEmployee(Employee employee) throws Exception{
        boolean isExisting = employeeDao.findById(employee.getId()).isPresent();
        if (isExisting) {
            return employeeDao.save(employee);
        } else {
            throw new Exception("Employee does not exist!");
        }
    }
}
