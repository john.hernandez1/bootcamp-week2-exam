package hk.com.novare.employee.profile.employee.profile.service;

import hk.com.novare.employee.profile.employee.profile.model.Employee;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface EmployeeDaoService {

    Employee addEmployee(Employee e);

    List<Employee> getAllEmployee();

    Employee getEmployeeById(int id) throws  Exception;

    int deleteEmployee(int id) throws Exception;

    Employee updateEmployee(Employee employee) throws Exception;

}
