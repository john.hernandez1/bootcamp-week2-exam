package hk.com.novare.employee.profile.employee.profile.service.impl;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import hk.com.novare.employee.profile.employee.profile.dao.EmployeeDao;
import hk.com.novare.employee.profile.employee.profile.model.Employee;

import java.util.List;
import java.util.Optional;

@ExtendWith(MockitoExtension.class)
class EmployeeDaoServiceImplTest {

    @Mock
    EmployeeDao repository;

    @InjectMocks
    EmployeeDaoServiceImpl employeeDaoService;

    List<Employee> testEmployee;
    Employee emp;

    @BeforeEach
    public void init() {
        emp = new Employee("Sample", "sample", 2);
        testEmployee = List.of(
                new Employee("Human", "sample name", 2),
                new Employee("Human2", "sample name", 5),
                new Employee("Human3", "sample name", 3)
        );
    }

    @Test
    @DisplayName("Should return list of Employees")
    void getAllEmployee() {
        Mockito.when(repository.findAll()).thenReturn(testEmployee);
        Assertions.assertEquals(testEmployee, employeeDaoService.getAllEmployee());
    }

    @Test
    @DisplayName("Should add new employee")
    void addEmployee() {
        Mockito.when(repository.save(emp)).thenReturn(emp);
        Assertions.assertEquals(emp, employeeDaoService.addEmployee(emp));
    }

    @Test
    @DisplayName("Should get employee by id")
    void getEmployeeById() throws  Exception{
        Mockito.when(repository.findById(emp.getId())).thenReturn(Optional.ofNullable(emp));
        Assertions.assertEquals(emp, employeeDaoService.getEmployeeById(emp.getId()));
    }
    @Test
    @DisplayName("Should display employee does not exist for find by id")
    void getEmployeeByIdFalse()
    {
        try{
            Mockito.when(repository.findById(emp.getId())).thenThrow(new Exception());
        } catch (Exception e) {
            Assertions.assertThrows(Exception.class,()->employeeDaoService.getEmployeeById(emp.getId()));
        }
    }

    @Test
    @DisplayName("Should delete employee")
    void deleteEmployee() throws Exception{
        Mockito.when(repository.findById(emp.getId())).thenReturn(Optional.ofNullable(emp));
        Assertions.assertEquals(emp.getId(), employeeDaoService.deleteEmployee(emp.getId()));
    }

    @Test
    @DisplayName("Should display employee does not exist for deletion")
    void deleteEmployeeFalse()throws Exception{
        try{
            Mockito.when(repository.findById(emp.getId())).thenThrow(new Exception());
        } catch (Exception e) {
            Assertions.assertThrows(Exception.class,()->employeeDaoService.deleteEmployee(emp.getId()));
        }
    }

    @Test
    @DisplayName("Should update employee")
    void updateEmployee() throws Exception {
        Mockito.when(repository.findById(emp.getId())).thenReturn(Optional.ofNullable(emp));
        Mockito.when(repository.save(emp)).thenReturn(emp);
        Assertions.assertEquals(emp, employeeDaoService.updateEmployee(emp));
    }
    @Test
    @DisplayName("Should display employee does not exist")
    void updateEmployeeFalse(){
        Employee nonExistingEmployee = new Employee();
        try{
            Mockito.when(repository.findById(emp.getId())).thenThrow(new Exception());
        } catch (Exception e) {
            Assertions.assertThrows(Exception.class,()->employeeDaoService.updateEmployee(nonExistingEmployee));
        }
    }
}